#!/bin/bash
echo "Dropping server packet"
# Dropping 80% of server packets
sudo tc qdisc change dev eno1 parent 1:1 handle 10: netem loss 80%
# Let the packet drop continue for the next 6 seconds
sleep 6
echo "Exiting packet loss"
# Stop packet drops
sudo tc qdisc change dev eno1 parent 1:1 handle 10: netem loss 1%
